@@include('alert.js');
function tesWebP(callback){
    let webP = new Image();
    webP.onload = webP.onerror = function (){
        callback(webP.height ==2);
    };
    webP.src = "data:image/webp;";
}
tesWebP(function (support){
    if (support ==true){
        document.querySelector("body").classList.add("webp");
    }
});